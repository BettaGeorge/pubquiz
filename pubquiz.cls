% Copyright 2022 Adrian Rettich
% 
% This file is part of the pubquiz LaTeX package.
% 
% This code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
% 
% This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License along with this code. If not, see <https://www.gnu.org/licenses/>.

\ProvidesExplClass{pubquiz}{2022/09/18}{0.1}{class for pub quiz slides}

\RequirePackage{tikz}
\RequirePackage{expl3} % not technically needed anymore, but does not hurt
\RequirePackage{l3keys2e}

% Wir definieren ein key-value-Interface für die Klasse.
\bool_new:N \l_onebyone_bool
\bool_new:N \l_oneanswer_bool
\bool_new:N \l_pause_bool
\bool_new:N \l_frame_bool
\tl_new:N \l_reveal_tl
\tl_new:N \l_background_tl
\tl_new:N \l_background_path_tl
\int_zero_new:N \l_correct_index_int
\keys_define:nn { pubquiz }
{
  % frame: automatically create a frame around the question.
  frame .bool_set:N = \l_frame_bool,
  frame .initial:n = { false },

  % background: any TeX code to be included in the background
  % of a quiz slide.
  background .value_required:n = true,
  background .code:n = {
    \tl_set:Nn \l_background_tl { #1 }
    \bool_set_true:N \l_frame_bool
  },

  % background file: a graphics file to be used as the background.
  background~file .value_required:n = true,
  background~file .code:n = {
    \tl_set:Nn \l_background_path_tl { #1 }
    \bool_set_true:N \l_frame_bool
  },

  % background opacity: only applies if background file is used.
  % Takes a value from 0 to 1.
  background~opacity .value_required:n = true,
  background~opacity .tl_set:N = \l_background_opacity_tl,
  background~opacity .initial:n = { 0.4 },

  % shuffle: if true, automatically shuffle the answers.
  shuffle .bool_set:N = \l_shuffle_bool,
  shuffle .initial:n = { true },

  % reveal: override the default overlay specification of
  % when to reveal the correct answer.
  % "false" means do not reveal.
  % anything else should be an overlay specification.
  reveal .code:n = {
    \tl_set:Nn \l_reveal_tl { #1 }
  },
  reveal .initial:n = { +(1)- },
  reveal .default:n =  { +(1)- },

  % overlay: pass an overlay specification to all answers.
  overlay .tl_set:N = \l_overlay_tl,
  overlay .value_required:n = true,
  overlay .initial:n = { 1- },

  % correct at: include the correct answer in a fixed position.
  % This option is only supported for single-answer questions.
  % For multiple correct answers in fixed positions, just turn
  % off shuffling and do it yourself.
  correct~at .value_required:n = true,
  correct~at .code:n = {
    \int_set:Nn \l_correct_index_int { #1 }
    \bool_set_true:N \l_oneanswer_bool
  },
  correct~at .initial:n = { 0 },

  % name: gives a name to this question to reference it later.
  name .value_required:n = true,
  name .tl_set:N = \l_name_tl,

  % one by one: reveal the possible answers one after the other
  % instead of all at once.
  % this one is a bit tricky: if one by one is specified,
  % we need to adjust the overlay spec of the "reveal" option
  % to the number of wrong answers. But at this point, we do not
  % know the number of wrong answers. So we set "reveal" to empty,
  % which the user can never do because it has .value_required,
  % then later check it for emptiness.
  one~by~one .code:n = {
    \str_if_eq:nnTF { #1 } { false }
    {
      \bool_set_false:N \l_onebyone_bool
    } {
      \bool_set_true:N \l_onebyone_bool
    }
    \bool_if:NT \l_onebyone_bool
    {
      \tl_set:Nn \l_reveal_tl {}
    }
  },
  one~by~one .default:n = {
    \bool_set_true:N \l_onebyone_bool
    \tl_set:Nn \l_reveal_tl {}
  },
  one~by~one .initial:n = { false },

  % pause: whether or not there should be a pause between the question
  % and the answers.
  pause .bool_set:N = \l_pause_bool,
  pause .initial:n = { false },

  % one answer: turn off parsing the correct answer as a comma-
  % separated list.
  one~answer .bool_set:N = \l_oneanswer_bool,
  one~answer .initial:n = { false },

  % one herring: same as one answer, but for wrong answers.
  one~herring .bool_set:N = \l_oneherring_bool,
  one~herring .initial:n = { false },

}

% Process the options given by the user to the class to overwrite the
% defaults above.
\ProcessKeysOptions { pubquiz }

\LoadClass{ beamer }

\cs_generate_variant:Nn \seq_set_map:NNn {Ncn}

% Messages for later use
\msg_new:nnnn { pubquiz } { question_name_taken } {
  A~question~with~the~name~#1~already~exists.~Ignoring~this~one.
} {
  You~have~used~the~same~name~(#1)~for~more~than~one~
  \textbackslash Quiz~or~\textbackslash QuizFrame~
  command,~but~question~names~must~be~unique.\\
  If~#1~consists~only~of~digits,~this~might~be~caused~by~
  you~issuing~more~than~one~\textbackslash Quiz~
  command~in~one~frame.~
  To~get~more~than~one~question~within~the~same~frame,~
  please~set~the~'name'~option~for~each.\\
  Another~source~for~this~error~might~be~that~you~manipulated~
  \textbackslash theframenumber.~Again,~you~can~solve~this~
  by~assigning~a~unique~name~to~the~question~in~question.\\
  If~you~continue,~I~shall~skip~typesetting~this~question.
}


% Ändere das folgende Makro, um zu spezifizieren, wie
% die richtige Antwort beim Reveal aussehen soll.
% nimmt als einziges Argument den Text, der formatiert
% werden soll.
\NewDocumentCommand\QuizCorrectAnswerFormat{+m}{
  \textcolor{blue}{\textbf{#1}}
}

% Die folgende Formatierung wird auf alle Antworten angewendet,
% ausser auf die richtige NACH dem Reveal.
% Vor dem Reveal sehen natürlich alle Antworten gleich aus.
\NewDocumentCommand\QuizAnswerFormat{+m}{
  #1
}

% Die folgende Formatierung wird auf die Frage angewendet.
\NewDocumentCommand\QuizQuestionFormat{+m}{
  \textbf{#1}
}

% Das beamer Package führt alle Befehle nicht nur
% auf jedem Frame neu aus, sondern auch einmal
% für jedes Overlay.
% Das hat zur Folge, dass, wenn wir die Antworten mischen,
% die Reihenfolge bei jedem Overlay geändert wird.
% Das ist offensichtlich Quatsch. Um das zu umgehen,
% mischen wir nicht die echte Antwortliste, sondern eine
% Liste von Indizes und merken uns diese Reihenfolge.
% Wir mischen dann nicht neu, bis sich der Frame ändert.
% Letzteres mache ich aktuell an \theframenumber fest, was
% sehr fragil klingt, aber ich habe noch keine bessere Idee
% gehabt.
% Insbesondere bedeutet das auch, dass man nicht mehr als
% einen \Quiz-Befehl pro Frame nutzen kann :(
% Addendum: Wir speichern die Reihenfolge jetzt nicht pro
% Frame, sondern pro named question.
% Insbesondere kann man dadurch beliebig viele Fragen auf
% einem Frame haben, solange sie alle named sind.
% \int_new:N \g_previous_frame_int
% \int_set:Nn \g_previous_frame_int {-1}
% \seq_new:N \g_quiz_seq
% Die andere Idee, einfach den fertigen Text zu speichern,
% damit man ihn nicht jedes Mal neu bauen muss, funktioniert
% nicht -- beamer macht dunkle Magie mit den Overlay Specifications
% und der Text geht kaputt, wenn man das versucht.

% #objectorientedlatex

% This is the constructor function for questions.
% #1: key-value options
% #2: name
% #3: question
% #4: comma-separated list of correct answers
% #5: comma-separated list of wrong answers
\cs_new:Nn \question_new:nnnnn {
  \prop_if_exist:cTF { pubquiz @ #2 }
  {
    \msg_error:nnn { pubquiz } { question_name_taken } { #2 }
  }{
    \prop_new:c { pubquiz @ #2 }
    \prop_gput:cnn { pubquiz @ #2 } { options } { #1 }
    \prop_gput:cnn { pubquiz @ #2 } { name } { #2 }
    \prop_gput:cnn { pubquiz @ #2 } { question } { #3 }

    % all comma-separated things are converted to sequences
    % before saving so we don't have to parse them every
    % time we access them.
    \seq_new:c { pubquiz_right @ #2 }
    \bool_if:NTF \l_oneanswer_bool
    {
      \seq_gput_right:cn { pubquiz_right @ #2 } { #4 }
    } {
      \clist_clear:N \l_tmpa_clist
      \clist_set:Nn \l_tmpa_clist { #4 }
      \seq_gset_from_clist:cN { pubquiz_right @ #2 } \l_tmpa_clist
    }

    \seq_new:c { pubquiz_wrong @ #2 }
    \bool_if:NTF \l_oneherring_bool
    {
      \seq_gput_right:cn { pubquiz_wrong @ #2 } { #5 }
    } {
      \clist_clear:N \l_tmpa_clist
      \clist_set:Nn \l_tmpa_clist { #5 }
      \seq_gset_from_clist:cN { pubquiz_wrong @ #2 } \l_tmpa_clist
    }

    % now that we have the answers saved, we shuffle them.
    % We leave the original sequences alone in case the
    % user wants to manipulate them again later.
    % to access the items in this sequence of "references",
    % do a \tl_use on them.
    \seq_new:c { pubquiz_answers @ #2 }

    % the correct answers are only inserted if the user did
    % not set the "correct at" option.
    \int_compare:nNnF { \l_correct_index_int } > { 0 }
    {
      \int_set:Nn \l_tmpa_int { 0 }
      \int_do_while:nNnn { \l_tmpa_int } < { \seq_count:c { pubquiz_right @ #2 } }
      {
        \int_incr:N \l_tmpa_int
        \tl_clear:N \l_tmpa_tl
        \tl_put_right:Nn \l_tmpa_tl  { \seq_item:cn { pubquiz_right @ #2 } }
        \tl_put_right:Nx \l_tmpa_tl  { { \int_use:N \l_tmpa_int } }
        \seq_gput_right:cV  { pubquiz_answers @ #2 } \l_tmpa_tl
      }
    }

    \int_set:Nn \l_tmpa_int { 0 }
    \int_do_while:nNnn { \l_tmpa_int } < { \seq_count:c { pubquiz_wrong @ #2 } }
    {
      \int_incr:N \l_tmpa_int
      \tl_clear:N \l_tmpa_tl
      \tl_put_right:Nn \l_tmpa_tl  { \seq_item:cn { pubquiz_wrong @ #2 } }
      \tl_put_right:Nx \l_tmpa_tl  { { \int_use:N \l_tmpa_int } }
      \seq_gput_right:cV  { pubquiz_answers @ #2 }  \l_tmpa_tl
    }

    % The references are saved. We shuffle the resulting list.
    % Note that this stops being able to reach all possible permutations
    % past 13 answers due to a limitation in TeX's RNG.
    % It fails entirely for more than 2^15 answers.
    \bool_if:NT \l_shuffle_bool
    {
      \seq_gshuffle:c { pubquiz_answers @ #2 }
    }

    % If "correct at" was not set, we are done.
    % Otherwise, insert the correct answer.
    \int_compare:nNnT { \l_correct_index_int } > { 0 }
    {
      \int_set:Nn \l_tmpa_int { 0 }
      \seq_clear:N \l_tmpa_seq
      \int_do_while:nNnn { \l_tmpa_int } < { \seq_count:c { pubquiz_wrong @ #2 } }
      {
        \int_incr:N \l_tmpa_int
        \int_compare:nNnT { \l_correct_index_int } = { \l_tmpa_int }
        {
          \tl_clear:N \l_tmpa_tl
          \tl_put_right:Nn \l_tmpa_tl  { \seq_item:cn { pubquiz_right @ #2 } { 1 } }
          \seq_put_right:NV \l_tmpa_seq \l_tmpa_tl
        }
        \seq_gpop_left:cN { pubquiz_answers @ #2 } \l_tmpa_tl
        \seq_put_right:NV \l_tmpa_seq \l_tmpa_tl
      }
      % if we did not get a hit so far, the user wants the correct
      % answer at the end.
      \int_compare:nNnT { \l_correct_index_int } > { \l_tmpa_int }
      {
        \tl_clear:N \l_tmpa_tl
        \tl_put_right:Nn \l_tmpa_tl  { \seq_item:cn { pubquiz_right @ #2 } { 1 } }
        \seq_put_right:NV \l_tmpa_seq \l_tmpa_tl
      }
      % Everything is sorted. Put it back in the global sequence.
      \seq_gset_eq:cN { pubquiz_answers @ #2 } \l_tmpa_seq
    }
  }
}
\cs_generate_variant:Nn \question_new:nnnnn { nx }

\cs_new:Nn \question_new:nnnn {
  \question_new:nnnnn {}{#1}{#2}{#3}{#4}
}

% \quiz is the main macro which can do literally everything this
% class has to offer. It takes the following arguments:
% #1: key-value options
% #2: question
% #3: comma-separated list of right answers
% #4: comma-separated list of wrong answers
% Whether the question and answers are typeset, whether the correct
% answers are revealed, and whether we are inside a frame or create our
% own frame is handled by the option interface.
% All specialised commands are simply wrappers for this one with
% preset keys.
\cs_new:Nn \quiz:nnnn {

  \keys_set:nn { pubquiz }{ #1 }

  % If the user provides no custom name, we use the frame number.
  % This breaks, of course, if the user manipulates that counter,
  % in which case we are screwed.
  % But honestly, if the user employs dark magic, they will just have
  % to set the names themselves.
  \tl_if_empty:NT \l_name_tl
  {
    \tl_set:Nx \l_name_tl { \theframenumber }
  }

  % now we check whether this is the first time we are trying to
  % compile this frame. Due to overlay magic, we need to check this
  % so the shuffle does not happen on each pause.
  \prop_if_exist:cTF { pubquiz @ \l_name_tl }
  {
    % if the question already exists, it might have some saved options.
    \prop_get:cnN { pubquiz @ \l_name_tl } { options }
    \keys_set:nV { pubquiz } \l_name_tl
    % Of course, since the user should be allowed to override the saved
    % options, we need to load the passed keys a SECOND time because
    % we might just have overwritten them by loading the saved ones.
    \keys_set:nn { pubquiz }{ #1 }
  }
  {
    \question_new:nxnnn { #1 } { \tl_use:N \l_name_tl } { #2 } { #3 } { #4 }
  }


  % One way or the other, the question exists now and we have the
  % correct options loaded.
  % The following sequence will hold the FORMATTED
  % answers, as opposed to the unformatted answers
  % in the two global sequences.
  \seq_clear_new:N \l_answers_seq
  \seq_set_eq:Nc \l_answers_seq { pubquiz_answers @ \l_name_tl }


  % Step one: wrap all answers in an \uncover if the user has
  % requested this.
  \seq_clear:N \l_tmpa_seq
  \bool_if:NTF \l_onebyone_bool
  {
    % one by one should override overlay.
    \seq_set_map:NNn \l_tmpa_seq \l_answers_seq
    {
      \uncover<+->{##1}
    }
    % also, we need to adjust the "reveal" option,
    % unless the user has overridden it by hand.
    \tl_if_empty:NT \l_reveal_tl
    {
      % we need to add 1 to reveal AFTER the last answer,
      % and another 1 to account for the question having a pause.
      % but \thebeamerpauses is always 1 more than the actual number
      % of pauses, so we subtract 1 again, yielding the following
      % code.
      \bool_if:NTF \l_pause_bool
      {
        \int_set:Nn \l_tmpa_int { \thebeamerpauses + 1 + \seq_count:N \l_answers_seq }
      }{
        \int_set:Nn \l_tmpa_int { \thebeamerpauses + \seq_count:N \l_answers_seq }
      }
      \tl_set:Nx \l_reveal_tl
      {
        \int_use:N \l_tmpa_int
        -
      }
    }
  }{
    \seq_set_map:NNn \l_tmpa_seq \l_answers_seq
    {
      \uncover<\l_overlay_tl>{##1}
    }
  }
  \seq_set_eq:NN \l_answers_seq \l_tmpa_seq

  % Step two: wrap the correct answers.
  % If "reveal" is anything but "false", the user has requested
  % to have the correct answers highlighted.
  \str_if_eq:VnTF \l_reveal_tl { false }
  {
    \seq_set_map:Ncn \l_tmpa_seq { pubquiz_right @ \l_name_tl }
    {
      { \item~\QuizAnswerFormat{##1} }
    }
  } {
    \seq_set_map:Ncn \l_tmpa_seq { pubquiz_right @ \l_name_tl }
    {
      \alt<\l_reveal_tl>
      { \alert { \item~\QuizCorrectAnswerFormat{##1} } }
      { \item~\QuizAnswerFormat{##1} }
    }
  }
  \seq_set_eq:cN { pubquiz_right @ \l_name_tl } \l_tmpa_seq


  % Step three: wrap the wrong answers.
  \seq_set_map:Ncn \l_tmpa_seq { pubquiz_wrong @ \l_name_tl }
  {
    { \item~\QuizAnswerFormat{##1} }
  }
  \seq_set_eq:cN { pubquiz_wrong @ \l_name_tl } \l_tmpa_seq

  
  % Step four: if the user has requested a frame with background,
  % we have to build the commands for that.
  \bool_if:NT \l_frame_bool
  {
    % FUN TIMES: the \setbeamertemplate macro breaks if used
    % inside ExplSyntax, so we have to expand everything and then
    % feed it to a helper function which turns ExplSyntax temporarily off.

    % If a file was provided, we simply override the "background" option
    % with it.
    \tl_if_empty:NF \l_background_path_tl
    {
      \tl_set:Nx \l_background_tl
      {
        \exp_not:N \begin
          {tikzpicture}
          \exp_not:N \node
          [
          opacity=\l_background_opacity_tl,
          inner~sep=0pt,
          ]
          {
            \exp_not:N \includegraphics
            [
            width=\exp_not:N \paperwidth
            ]
            {
              \l_background_path_tl
            }
          };
          \exp_not:N \end
        {tikzpicture}
      }
    }

    % \l_background_tl is now:
    % empty if no background was requested
    % custom code if "background" was set
    % the includegraphics code above if "background file" was set.
    \tl_if_empty:NF \l_background_tl
    {
      \pubquiz@beamertemplatewrapper{\tl_use:N \l_background_tl}
    }
  }


  % Step five: actually typeset stuff.
  % Obviously, it is terrible to replicate the entire code once
  % for frame=false and once for frame=true.
  % However, the frame environment breaks very easily, and putting
  % everything into a token list did not work.
  % This is the best solution with which I was able to come up
  % that actually works with frames.
  \bool_if:NTF \l_frame_bool
  {
    % FUN TIMES: \begin{frame} and \end{frame} break if used inside
    % ExplSyntax.
    \ExplSyntaxOff
    \begin{frame}
      \ExplSyntaxOn

      % The question.
      \prop_get:cnN { pubquiz @ \l_name_tl } { question } \l_tmpa_tl
      \QuizQuestionFormat{
        \l_tmpa_tl
      }

      % if "one by one" is set or if the user has requested a pause manually,
      % the first answer should not pop up right with the question.
      \bool_if:NT \l_pause_bool
      {
        \pause
      }

      % The answers.
      \begin{enumerate}
        \seq_use:Nn \l_answers_seq {\\}
      \end{enumerate}
      

      \ExplSyntaxOff
    \end{frame}
    \ExplSyntaxOn
  }{
    % The question.
    \prop_get:cnN { pubquiz @ \l_name_tl } { question } \l_tmpa_tl
    \QuizQuestionFormat{
      \l_tmpa_tl
    }

    % if "one by one" is set or if the user has requested a pause manually,
    % the first answer should not pop up right with the question.
    \bool_if:NT \l_pause_bool
    {
      \pause
    }

    % The answers.
    \begin{enumerate}
      \seq_use:Nn \l_answers_seq {\\}
    \end{enumerate}
  }

  % CAVEAT: once we are done, we HAVE to reset the beamer canvas
  % if we changed it.
  % Otherwise, beamer keeps trying to use the local variables from
  % above, which no longer exist.
  % TODO: save the previous canvas and reset to it here
  % rather than just emptying it out and leaving the user to
  % deal with it.
  \tl_if_empty:NT \l_background_tl
  {
    \pubquiz@beamertemplatewrapper{}
  }


}

% As explained above, this helper function exists solely
% because \setbeamertemplate breaks inside ExplSyntax.
\NewDocumentCommand\pubquiz@beamertemplatewrapper{m}
{
  % \typeout{#1}
  \ExplSyntaxOff
  \setbeamertemplate{background~canvas}{
    #1
  }
  \ExplSyntaxOn
}


% This concludes the part of the class with actual content.
% From here on, we do nothing except introduce several friendlier
% wrappers for \quiz.

% \Quiz:
% This is a vanilla wrapper which simply makes the expl3 macro
% available in latex2e.
% As is customary, the key-value options are optional rather than mandatory.
\NewDocumentCommand\Quiz{ O{} +m +m +m }
{
  \quiz:nnnn{#1}{#2}{#3}{#4}
}


\NewDocumentCommand\Reveal{ O{} +m +m +m }
{
  \quiz:nnnn{#1, reveal}{#2}{#3}{#4}
}








