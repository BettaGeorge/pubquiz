% Copyright 2022 Adrian Rettich
% 
% This file is part of the pubquiz LaTeX package.
% 
% This code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
% 
% This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License along with this code. If not, see <https://www.gnu.org/licenses/>.
\documentclass{beamer}

\usepackage{pubquiz}

\newcommand\tq{\texttt{\textbackslash Quiz}}
\newcommand\tqf{\texttt{\textbackslash Quizframe}}
\newcommand\co[1]{\texttt{\textbackslash #1}}
\let\tb\textbackslash

\newenvironment{werbatim}{
\tt
}{}

\begin{document}

\begin{frame}
  \frametitle{TEST}
  \begin{itemize}
  \item <alert@2-> foo
  \end{itemize}
\end{frame}

\section{Introduction}
\begin{frame}
  \frametitle{The Pubquiz Package}
  This package provides two commands:

  \begin{werbatim}
 \textbackslash Quiz
\end{werbatim}

  can be used anywhere in a frame to typeset a question with several answer possibilities and eventually reveal the correct answer.

  \begin{werbatim}
 \tb Quizframe
\end{werbatim}

is used without a surrounding frame and creates its own frame for the question.
\end{frame}

\section{The Quiz Command}
\begin{frame}
  \frametitle{The \texttt{\textbackslash Quiz} Command}
\begin{werbatim}
\tb Quiz

\{What is the answer to the question of life,
the universe, and everything?\}

\{42\}

\{yes, \{Life, The Universe, And Everything\}, cheese\}
 \end{werbatim}
  
  \Quiz{What is the answer to the question of life, the universe, and everything?}{42}{yes,{Life, The Universe, And Everything}, cheese}

  \only<1>{    In its simplest form, the \tq{} command takes three mandatory arguments:
    a question, the correct answer,
    and a comma-separated list of wrong answers.

    The correct answer is automatically highlighted on the next slide.
  }
  \only<2>{
    Notice how we have escaped a comma occurring in one of the answers by putting the whole answer in braces.
    Without them, ``Life, The Universe, And Everything'' would be parsed as three separate answers ``Life'', ``The Universe'', and ``And Everything''.
  }
  \only<3>{
    Notice further that the command has automatically put the answers in a random order to prevent you from accidentally creating discernible patterns.

    This behaviour can be changed via the options explained on the following slides.
  }
\end{frame}

\begin{frame}
  \frametitle{Sorting Options}
  The \tq{} command can be used with an optional, comma-separated list of key-value style parameters as its first argument:

  \begin{werbatim}
\tb Quiz [

shuffle=false,

correct at=3

]

\{What do I have in my pocket?\}

\{A Ring\}

\{My Hand, Nothing\}
\end{werbatim}

\Quiz [
  shuffle=false,
  correct at=3
  ]
  {What do I have in my pocket?}
  {A Ring}
  {My Hand, Nothing}
\end{frame}


\begin{frame}
  \frametitle{Shuffling Answers}
  As showcased on the previous slide, the \texttt{shuffle} option can be used to present the answers in exactly the order in which they are given to the \tq{} command by specifying \texttt{shuffle=false}.

  Specifying \texttt{shuffle} without a value or with the value \texttt{shuffle=true} turns shuffling on, which is the default.

  If shuffling is on, then the correct answer is inserted in a random position among the wrong answers.
  This behaviour can be turned off as explained on the following slide.
\end{frame}

\begin{frame}
  \frametitle{Positioning the Correct Answer}
  The \texttt{correct at} option overrides automatic positioning of the correct answer.
  It takes a positive integer as its argument and puts the correct answer in exactly the specified position.
  For example, \texttt{correct at=1} puts the correct answer first.

  Any value larger than the number of available answers will simply put the correct answer last.

  If the \texttt{correct at} option is used together with \texttt{shuffle=true} (the default), then the wrong answers are printed in a random order with the correct one inserted at the desired position.
\end{frame}

\begin{frame}
  \frametitle{Overlay Specifications}
  By default, all answers are printed immediately, with the correct answer revealed on the next slide.

  Two options are available to change this behaviour.
  
\begin{werbatim}
  \tb Quiz [
  
overlay=<+->,

reveal=<4->

]

\{What do I have in my pocket?\tb pause\}

\{A Ring\}

\{My Hand, Nothing\}
\end{werbatim}

\Quiz [
overlay=<+->,
reveal=<5->
]
{What do I have in my pocket?\pause}
{A Ring}
{My Hand, Nothing}
\end{frame}

\begin{frame}
  \frametitle{Overlay Specifications for Answers}
  Answers are items in an \texttt{itemize} environment.
  Use the \texttt{overlay} option to pass an overlay specification to each item.
  Consult the \texttt{beamer} documentation for how to use overlay specifications.

  The correct answer is revealed at the overlay specification \texttt{reveal}, which defaults to \texttt{<+->}.
\end{frame}

\begin{frame}
  \frametitle{Formatting}
  In order to format questions and answers, one should use \co{renewcommand} on the following commands.

  Each of them takes one mandatory argument, which provides access to the text one wants to format.

  \begin{itemize}
  \item \co{QuizQuestionFormat[1]} is applied to the question text.
  \item \co{QuizAnswerFormat[1]} is applied to the text of every answer, including the correct answer before it is revealed.
  \item \co{QuizCorrectAnswerFormat[1]} is applied to the text of the correct answer, but only \emph{after} it is revealed. Once revealed, \co{QuizAnswerFormat} no longer applies.
  \end{itemize}
\end{frame}


 \renewcommand\QuizAnswerFormat[1]{
   \textit{#1}
 }

\begin{frame}
   \frametitle{A Formatting Example}
   The quiz on this slide was produced by issuing the following:


   \begin{werbatim}

     \tb renewcommand\tb QuizAnswerFormat[1]\{

     \tb textit\{\#1\}

     \}
   \end{werbatim}
   
  \Quiz
   {What do I have in my pocket?}
   {A Ring}
   {My Hand, Nothing}

 \pause
 Note how the revealed answer is no longer in italics.

 \pause
 Note also that due to idiosyncrasies of the \texttt{beamer} package, the \co{renewcommand} should be issued \emph{outside} a frame environment.
 Issuing it inside a frame may or may not work and can produce very arcane error messages.
\end{frame}

\renewcommand\QuizAnswerFormat[1]{#1}

\begin{frame}
  \frametitle{More Than One Quiz On A Slide}
  It is possible to issue more than one \tq{} command in the same frame.

  However, you can currently only do this if the number of answers is the same, and things might break in unexpected ways. Use at your own risk.

  \Quiz
  {What do I have in my pocket?}
  {A Ring}
  {My Hand, Nothing}

  \Quiz
  {What is the answer to the question of life, the universe, and everything?}{42}{yes,{Life, The Universe, And Everything}}

\end{frame}

\section{The QuizFrame Command}
 \begin{frame}
  \frametitle{The \tqf{} Command}
\begin{werbatim}
\tb QuizFrame

\{What is the answer to the question of life,
the universe, and everything?\}

\{42\}

\{yes, \{Life, The Universe, And Everything\}, cheese\}
\end{werbatim}

  The \tqf{} command takes the same arguments as the \tq{} command,
  and you can pass options in the same way as explained there.
  It understands all the same options, plus additional ones explained on the coming slides.

  If used without options, it behaves like the \tq{} command, except that it encloses the quiz in a
  
 \begin{werbatim}
   \tb begin\{frame\}
   
 \tb end\{frame\}
\end{werbatim}

  environment and must therefore be used \emph{outside} a frame.
  
\end{frame}

\QuizFrame
{What is the answer to the question of life,
the universe, and everything?}
{42}
{yes, {Life, The Universe, And Everything}, cheese}

 \begin{frame}
  \frametitle{Inserting A Background Image}
\begin{werbatim}
  \tb QuizFrame[
  
  background file=galaxy,
  
  background opacity=.4
  
  ]
  
\{What is the answer to the question of life,
the universe, and everything?\}

\{42\}

\{yes, \{Life, The Universe, And Everything\}, cheese\}
\end{werbatim}

  The above takes any file that can be passed to \co{includegraphics} and sets it as the background picture.

  In order to make the text more readable, you can set a \texttt{background opacity} between 0 and 1, where 0 is invisible and 1 is fully opaque.
  The default is 0.4.

\end{frame}

\QuizFrame[
background file=galaxy,
background opacity=.4
]
{What is the answer to the question of life,
the universe, and everything?}
{42}
{yes, {Life, The Universe, And Everything}, cheese}
 
\begin{frame}
  \frametitle{Inserting Different Backgrounds}
  \begin{werbatim}
    \tb QuizFrame[

    background=\{
    
    \tb tikz \{

    \tb draw (0,0) grid (15,15);

    \}
    \}
    
  ]
\{What is the answer to the question of life,
the universe, and everything?\}
\{42\}
\{yes, \{Life, The Universe, And Everything\}, cheese\}
  \end{werbatim}

  If more than a simple background image is desired, the \texttt{background} option takes \emph{any} object that should be used as the backdrop.
  Complicated objects should be escaped by enclosing them in braces.
  
  This option does nothing more than pass the object to \texttt{beamer}s \co{setbeamertemplate} command.
\end{frame}

\QuizFrame[
background={
\tikz{\draw (0,0) grid (15,15);}
}
]
{What is the answer to the question of life,
the universe, and everything?}
{42}
{yes, {Life, The Universe, And Everything}, cheese}

\begin{frame}
  \frametitle{Copyright Information}
  \tt
  Copyright 2022 Adrian Rettich

This file is part of the pubquiz LaTeX package.

This code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this code. If not, see <https://www.gnu.org/licenses/>.

\end{frame}


\end{document}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
